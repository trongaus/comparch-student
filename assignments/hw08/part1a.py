#!/usr/bin/python
import os
import csv
import math

# use nested for loops to sweep through the different simulation parameters
# capacity = associativity * sets * block size
# need to vary capacity from 32 -> 4096
# need to vary block size from 1 word (4 bytes) to 32 words
# log2-sets-per-way = log 2 (capacity / block size)
# log2-bytes-per-block = log 2 (block size * 4)

# first: miss rate vs. block size
associativity = 1
for cap in (32, 64, 128, 256, 512, 1024, 2048, 4096):
	for b_size in (1, 2, 4, 8, 16, 32):
		log2_sets = int(math.log(cap/b_size,2))
		log2_bytes_per_block = int(math.log(b_size*4, 2))

		# set up a string with the isimp command line and do an OS call to run isimp
		cmd = ('isimp -dcache ' +
				str(associativity) + ' ' +
				str(log2_sets) + ' ' +
				str(log2_bytes_per_block) +
				' matrix-mult')
		print cmd
		os.system(cmd)

		# read the stats.csv file into a python dictionary and then get the miss rate
		f = open('stats.csv')
		reader = csv.reader(f)
		my_dict = dict(reader)

		# store the miss rates in a vector or matrix for plotting
		miss_rate = float(my_dict['dCacheMissRate'])
		print miss_rate

# plot the results directly from python or export the data to a text file for plotting in another application (i.e., a spreadsheet)
