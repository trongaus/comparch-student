# prob1_partA.s
	.set noreorder
	.data
	.text
    .globl main
    .ent main
    
main:
		addi $s0, $0, 84			# score = 84
		slti $t0, $s0, 90				# if (score >= 90)
		bne $t0, $0, elseif1		# if (t0) goto elseif1
		nop
		addi $s1, $0, 4				# grade = 4
		j print							# jump to print block
		
elseif1:
		slti $t1, $s0, 80				# else if (score >= 80)
		bne $t1, $0, elseif2		# if !(t1) goto elseif2
		nop
		addi $s1, $0, 3				# grade = 3
		j print							# jump to print block
		
elseif2:
		slti $t1, $s0, 70				# else if (score >= 70)
		bne $t2, $0, else			# if !(t2) goto else
		nop
		addi $s1, $0, 2				# grade = 2
		j print							# jump to print block
		
else:
		addi $s1, $0, 0				# else grade = 0
		j print							# jump to print block

print:
		add $a0, $0, $s1			# print grade in hex and dec
		ori $v0, $0, 20				# syscall print code 20
		syscall		
		nop
		
        ori $v0, $0, 10				# exit
        syscall
    .end main

