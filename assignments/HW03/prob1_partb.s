# prob1_partB.s
	.set noreorder
	.data
	.text
	.globl print
	.ent print

print: 
		ori $v0, $0, 20			# syscall print code 20
		syscall
		jr $ra
		nop
		.end print
		.globl main
		.ent main

main:
		# PUSH s0 (36 bytes)
		addi $sp, $sp, -36		# allocate 36 spots in the array
		sw $ra, 32($sp)
		
		add $s0, $0, $sp		# s1 = A
		addi $s1, $0, 0			# A[0] = 0
		sw $s1, 0($s0)			# $s1 = memory($s0 + 0)
		addi $s2, $0, 1			# A[1] = 1
		sw $s2, 4($s0)			# $s2 = memory($s0 + 4)
		addi $s3, $0, 2			# i = 2

loop_condition:
		slti $t1, $s3, 8			# t1 = (i < 8)
		beq $t1, $0, end		# if t1 = 0, goto end
		nop
		add $s4, $s2, $s1		# A[i] = A[i-1] + A[i-2]
		sw $s4, 0($s0)			# $s4 = memory($s0 + 0)
		add $s1, $0, $s2		# shift forward our int counters ($s1 = $s2 = A[1])
		add $s2, $0, $s4		# same as above ($s2 = $s4 = A[2])
		addi $s3, $s3, 1		# i++
		
		# CALL PRINT FUNCTION
		addi $a0, $s4, 0		# load $s4 for print
		jal print	
		nop
		j loop_condition
		nop
		
end:
		# POP s0 (32 bytes)
		lw $ra, 32($sp)
		addi $sp, $sp, 36
		jr $ra
		nop
		ori $v0, $0, 10			# syscall code 10 for exit
		syscall
	.end main
