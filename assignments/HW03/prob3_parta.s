# prob3_partA.s
	.set noreorder
	.data
A: .space 32
	.text
    .globl main
    .ent main
    
main:
		la $s0, A					# load array A into $s0
		addi $s1, $0, 0			# A[0] = 0
		sw $s1, 0($s0)			# $s1 = memory($s0 + 0)
		addi $s2, $0, 1			# A[1] = 1
		sw $s2, 4($s0)			# $s2 = memory($s0 + 4)
		addi $s0, $s0, 4		# advance to the next int in memory
		addi $s3, $0, 2			# i = 2

loop_condition:
		slti $t1, $s3, 8			# t1 = (i < 8)
		beq $t1, $0, end		# if t1 = 0, goto end
		nop
		
		add $s4, $s2, $s1		# A[i] = A[i-1] + A[i-2]
		addi $s0, $s0, 4		# advance to the next int in memory
		sw $s4, 0($s0)			# $s4 = memory($s0 + 0)
		add $s1, $0, $s2		# shift forward our int counters ($s1 = $s2 = A[1])
		add $s2, $0, $s4		# same as above ($s2 = $s4 = A[2])
		addi $s3, $s3, 1		# i++
		
		add $a0, $0, $s4		# print A[i] in hex and decimal
		ori $v0, $0, 20			# syscall print code 20
		syscall
		nop
		j loop_condition
		nop

end:
        ori $v0, $0, 10			# exit
        syscall
    .end main
