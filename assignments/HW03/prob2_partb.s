# prob2_partB.s
	.set noreorder
	.data
	.text
    .globl print
    .ent print
    
print: 
		ori $v0, $0, 20			# syscall code 20 for print
		syscall
		jr $ra
		nop
	.end print
	.globl sum3
	.ent sum3

sum3:								# int sum3(int a, int b, int c) 
		add $t7, $a0, $a1		# $t7 = $a0 + $a1 (sum = a + b)
		add $t7, $t7, $a2		# $t7 = $t7 + $a2	(sum = sum + c)
		add $a3, $0, $t7		# $a3 = $0 + $t7 (store sum in $a3)
		ori $v0, $0, 20			# return a+b+c
		jr $ra
		nop
	.end sum3
	.globl polynomial
	.ent polynomial
	
polynomial:
										# int polynomial(int a, int b, int c, int d, int e)
		sllv $t0, $a0, $a1		# x = a << b ($t0 = $a0 << $a1)
		sllv $t1, $a2, $a3		# y = c << d ($t1 = $a2 << $a3)
		addi $a0, $t0, 0		# store $t0 (x) in $a0
		addi $a1, $t1, 0		# store $t1 (y) in $a1
		addi $a2, $t4, 0		# store $t4 (e) in $a2
		jal sum3
		nop
		addi $t2, $a3, 0		# store value of sum ($a3) in $t2
		
		addi $a0, $t0, 0		# store $t0 in $a0 to make print call
		jal print
		nop
		
		addi $a0, $t1, 0		# store $t1 in $a0 to make print call
		jal print
		nop
		
		addi $a0, $a3, 0		# store $t2 in $a0 to make print call
		jal print
		nop
		jr $t9
		nop
	.end polynomial
	.globl main
	.ent main
		
main:
		addi $s0, $0, 2			# int a = 2 
		add $a0, $0, $s0		# $a0 = $s0
		addi $a1, $0, 3			# $a1 = 3
		addi $a2, $0, 4			# $a2 = 4
		addi $a3, $0, 5			# $a3 = 5
		addi $t4, $0, 6			# $t4 = 6 -- ran out of argument registers
		jal polynomial				# int f = polynomial(a, 3, 4, 5, 6);
		addi $t9, $ra, 0	
		addi $t5, $a0, 0
		
		addi $a0, $s0, 0		# print(a)
		jal print
		nop
		
		addi $a0, $t2, 0		# print(z)
		jal print
		nop
		
        ori $v0, $0, 10			# syscall code 10 for exit
        syscall
    .end main
    
