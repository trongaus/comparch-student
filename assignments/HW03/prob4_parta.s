# prob4_partA.s
	.set noreorder
	.data
record:											# create typedef struct record
	record.field0: .space 4				# allocate 4 bytes for each int field0
	record.field1: .space 4				# same as above for field1
	.text
    .globl main
    .ent main
    
main:
		addi $a0, $0, 8						# set size of record to 8 (2 ints of 4 bytes)
		ori $v0, $0, 9						# syscall code 9 for malloc
		syscall
		addi $s0, $v0, 0					# store address from $v0 in $s0
		
		addi $t0, $0, 0						# set $t0 as initial location
		addi $t1, $0, 100					# set $t1 to 100
		sw $t1, record($t0)					# r->field0 = 100
		
		addi $t0, $t0, 4						# shift 4 to next location
		addi $t1, $0, -1						# set $t1 to -1
		sw $t1, record($t0)					# store -1 in the next field

        ori $v0, $0, 10  					# syscall exit code 10
        syscall
    .end main
