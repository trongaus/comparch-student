# prob5_partA.s
	.set noreorder
	.data
	.text
    .globl main
    .ent main
    
main:
										# newelt = (elt *) MALLOC(sizeof (elt));
		addi $a0, $0, 8			# set size of elt to 8 bytes
		ori $v0, $0, 9			# syscall code 9 for malloc
		syscall
										# newelt->value = 1;
		addi $t0, $0, 1			# $t0 = 1
		sw $t0, 0($v0)			# $t0 = memory($v0 + 0)
		
										# newelt->next = 0;
		addi $v0, $v0, 4		# $v0 += 4
		addi $t1, $0, 0			# $t1 = 0
		sw $t1, 0($v0)			# $t1 = memory($v0 + 0)
		
										# head = newelt;
		addi $v0, $v0, -4		# $v0 += -4
		add $v1, $0, $v0		# $v1 = $v0
		
										# newelt = (elt *) MALLOC(sizeof (elt));
		addi $a0, $0, 8			# set size of elt to 8 bytes
		ori $v0, $0, 9			# syscall code 9 for malloc
		syscall
										# newelt->value = 2;
		addi $t2, $0, 2			# $t2 = 2
		sw $t2, 0($v0)			# $t2 = memory($v0 + 0)
		
										# newelt->next = head;
		addi $v0, $v0, 4		# $v0 += 4
		sw $v1, 0($v0)			# $v1 = memory($v0 + 1)
										
										# head = newelt
		addi $v0, $v0, -4		# $v0 += -4
		add $a1, $8, $v0		# $a1 = $v0
		
										# PRINT_HEX_DEC(head->value);
		lw $t3, 0($a1)			# load $a1 into $t3
		add $a0, $t3, $8		# $a0 = $t3
		ori $v0, $0, 20			# syscall code 20 for print
		syscall
										# PRINT_HEX_DEC(head->next->value);
		addi $a1, $a1, 4		# $a1 += 4
		lw $t4, 0($a1)			# load $a1 into $t4
		add $a0, $t4, $0		# $a0 = $t4
		ori $v0, $0, 20			# syscall code 20 for print
		syscall

        ori $v0, $0, 10			# syscall code 10 for exit
        syscall
    .end main

