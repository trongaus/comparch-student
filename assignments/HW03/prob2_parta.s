# prob2_partA.s
	.set noreorder
	.data
A: .byte 1, 25, 7, 9, -1			# char A[] = {1, 25, 7, 9, -1}
	.text
    .globl main
    .ent main
    
main:
		la $s0, A						# load A into s0
		addi $s1, $0, 0				# load i=0 into s1
		lb $s2, 0($s0)				# load byte 0 of A into s2 as current
		add $s3, $0, $0			# load max=0 into s3
		
loop_condition:
		slt $t0, $0, $s2				# while (current > 0)
		beq $t0, $0, end_while	# if t0 = 0, goto end_while
		nop
		slt $t1, $s3, $s2			# if (current > max)
		beq $t1, $0, end_if		# if t1 = 0, goto end_if
		nop
		add $s3, $0, $s2			# max = current
		
end_if:
		addi $s1, $s1, 1			# i = i + 1
		addi $s0, $s0, 1			# current = A[i]
		lb $s2, 0($s0)				# load in the next char in the array
		j loop_condition				# jump to loop_condition
		nop
		
end_while:
		add $a0, $0, $s3			# print in hex and dec
		ori $v0, $0, 20				# syscall print code 20
		syscall		
		nop

        ori $v0, $0, 10				# exit
        syscall
    .end main

